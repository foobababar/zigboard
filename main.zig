// references :
// https://discord.com/channels/605571803288698900/605572581046747136/910997162358358098
// super tuto on making a text editor, entering raw mode : https://viewsourcecode.org/snaptoken/kilo/


const std = @import("std");
const os = std.os;
const system = std.os.system;
const linux = os.linux;
const print = std.debug.print;
const Timer = std.time.Timer;


const SEC_IN_NANO = 100000000;




// ~~~~ ENTER/EXIT RAW MODE ~~~~
const C_VMIN = 6;
const C_VTIME = 5;
fn enable_raw_mode() !void {

  var termios = try os.tcgetattr(os.STDIN_FILENO);

  termios.iflag &= ~(@as(u16, linux.BRKINT | linux.ICRNL | linux.INPCK | linux.ISTRIP | linux.IXON));
  termios.cflag &= ~(@as(u16, linux.OPOST));
  termios.cflag |=(linux.CS8);
  termios.lflag &=~(@as(u16, linux.ECHO | linux.ICANON | linux.ISIG | linux.ISIG));

  termios.cc[C_VMIN] = 0;
  termios.cc[C_VTIME] = 1;

  try os.tcsetattr(os.STDIN_FILENO, linux.TCSA.FLUSH, termios);
}


fn disable_raw_mode(orig_termios: *os.termios) !void {
    try os.tcsetattr(os.STDIN_FILENO, linux.TCSA.FLUSH, orig_termios.*);
}



// ~~~~ GET TERMINAL DIMENSIONS ~~~~
// this function comes from here : https://discord.com/channels/605571803288698900/1100076867966472304/1100079161634209933
fn get_terminal_dimensions() !struct{x: u32, y:u32} {

    var size = std.mem.zeroes(linux.winsize);
    const err = os.system.ioctl(std.io.getStdIn().handle, linux.T.IOCGWINSZ, @ptrToInt(&size));

    if (os.errno(err) != .SUCCESS) {
        return os.unexpectedErrno(@intToEnum(os.system.E, err));
    }

    return .{
        .x=size.ws_col, 
        .y=size.ws_row,
    };
}

















// ~~~~ CURSOR MOVEMENT, ANSI STUFF
//
// ANSI escape codes
// ANSI escape sequences
// https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797


// put cursor at its starting position : top left of the screen
fn cursor_home() !void {
    const stdout = std.io.getStdOut().writer();
    try stdout.print("{s}", .{"\x1b[H"});
    // std.debug.print("{}, {any}", .{@TypeOf(stdout), stdout});
}



fn clear_screen() !void {
    const stdout = std.io.getStdOut().writer();
    try stdout.print("{s}", .{"\x1b[2J"});
}


fn cursor_to(x: u32, y: u32) !void {
    const stdout = std.io.getStdOut().writer();
    try stdout.print("\x1b[{d};{d}H", .{y, x});
}

fn refresh_screen(screen: []u8) !void {
    const stdout = std.io.getStdOut().writer();
    try stdout.print("{s}", .{screen});
}


fn fill_screen(screen: []u8, pattern: u8) void {
    for(screen, 0..)|_, i|{
        screen[i] = pattern; 
    }
}










pub fn main() !void {

    // ~~~~ allocator stuff ~~~~
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();
    defer _ = gpa.deinit();


    const terminal_dim = try get_terminal_dimensions();
    // print("terminal dimensions are : {d} {d} \n", .{terminal_dim.x, terminal_dim.y});

    var cursor_x: u32 = 0;
    var cursor_y: u32 = 0;
    var refresh_asked: bool = true;
    const delta = 2;

    const stdin = std.io.getStdIn().reader();

    var orig_termios = try os.tcgetattr(os.STDIN_FILENO);

    try enable_raw_mode();

    var screen = try allocator.alloc(u8, terminal_dim.x*terminal_dim.y);
    defer allocator.free(screen);
    fill_screen(screen, '*');


    // ~~~~ timer stuff ~~~~
    // var timer = try Timer.start();
    // var time_now = timer.read();


    // ~~~~ main loop ~~~~
    while(true){

        // const time_0 = timer.read();

        const byte = stdin.readByte() catch 0; 
        switch(byte) {

             0 => { //nothing pressed
             },

            'q' => {
                try disable_raw_mode(&orig_termios);
                std.os.exit(1);
            },

            'c' => {
                try clear_screen();
                refresh_asked = true;
            },

            'r' => {
                try  cursor_home(); 
                refresh_asked = true;
            },

            'h' => {
                if(cursor_x>=delta) cursor_x-=delta; 
                refresh_asked = true; 
            },

            'j' => {
                if(cursor_y<terminal_dim.y) cursor_y+=delta;
                refresh_asked = true;
            },

            'k' => {
                if(cursor_y>=delta) cursor_y -= delta;
                refresh_asked = true;
            },

            'l' => {
                if(cursor_x<terminal_dim.x) cursor_x+=delta;
                refresh_asked = true;
            },

            else => {
            },
        }
        // const time_1 = timer.read();

        if(refresh_asked){
            try refresh_screen(screen);
            try cursor_to(cursor_x, cursor_y); 
        }
    }
    try disable_raw_mode(&orig_termios);
}


